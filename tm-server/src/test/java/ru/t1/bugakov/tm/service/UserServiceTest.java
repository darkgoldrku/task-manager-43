package ru.t1.bugakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.bugakov.tm.api.service.IConnectionService;
import ru.t1.bugakov.tm.api.service.IPropertyService;
import ru.t1.bugakov.tm.api.service.dto.IProjectDTOService;
import ru.t1.bugakov.tm.api.service.dto.ITaskDTOService;
import ru.t1.bugakov.tm.api.service.dto.IUserDTOService;
import ru.t1.bugakov.tm.dto.model.UserDTO;
import ru.t1.bugakov.tm.enumerated.Role;
import ru.t1.bugakov.tm.exception.entity.UserNotFoundException;
import ru.t1.bugakov.tm.exception.field.EmailEmptyException;
import ru.t1.bugakov.tm.exception.field.LoginEmptyException;
import ru.t1.bugakov.tm.exception.field.PasswordEmptyException;
import ru.t1.bugakov.tm.service.dto.ProjectDTOService;
import ru.t1.bugakov.tm.service.dto.TaskDTOService;
import ru.t1.bugakov.tm.service.dto.UserDTOService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserServiceTest {

    @NotNull
    private IUserDTOService userService;

    @NotNull
    private List<UserDTO> userList;

    @Before
    public void initService() throws SQLException {
        userList = new ArrayList<>();
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        @NotNull final IProjectDTOService projectService = new ProjectDTOService(connectionService);
        @NotNull final ITaskDTOService taskService = new TaskDTOService(connectionService);
        userService = new UserDTOService(connectionService, projectService, taskService, propertyService);
        @NotNull final UserDTO admin = new UserDTO("admin", "admin", "admin@admin");
        @NotNull final UserDTO user = new UserDTO("user", "user", "user@user");
        @NotNull final UserDTO cat = new UserDTO("cat", "cat", "cat@cat");
        @NotNull final UserDTO mouse = new UserDTO("mouse", "mouse", "mouse@mouse");
        userList.add(admin);
        userList.add(user);
        userList.add(cat);
        userList.add(mouse);
        userService.add(admin);
        userService.add(user);
        userService.add(cat);
        userService.add(mouse);
    }

    @Test
    public void testCreate() throws SQLException {
        Assert.assertEquals(4, userService.getSize());
        userService.create("dog", "dog");
        Assert.assertEquals(5, userService.getSize());
        userService.create("fox", "fox", "fox@fox");
        Assert.assertEquals(6, userService.getSize());
        userService.create("lion", "lion", Role.ADMIN);
        Assert.assertEquals(7, userService.getSize());
    }

    @Test(expected = LoginEmptyException.class)
    public void testCreateEmptyLogin() throws SQLException {
        userService.create("", "dog");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testCreateEmptyPassword() throws SQLException {
        userService.create("dog", "");
    }

    @Test(expected = EmailEmptyException.class)
    public void testCreateEmptyEmail() throws SQLException {
        userService.create("dog", "dog", "");
    }

    @Test
    public void testFindByLoginPositive() throws SQLException {
        for (@NotNull final UserDTO user : userList) {
            Assert.assertNotNull(userService.findByLogin(user.getLogin()));
            Assert.assertEquals(user, userService.findByLogin(user.getLogin()));
        }
    }

    @Test
    public void testFindByLoginNegative() throws SQLException {
        @NotNull final String login = "test_login";
        Assert.assertNull(userService.findById(login));
    }

    @Test
    public void testFindByEmailPositive() throws SQLException {
        for (@NotNull final UserDTO user : userList) {
            Assert.assertNotNull(userService.findByEmail(user.getEmail()));
            Assert.assertEquals(user, userService.findByEmail(user.getEmail()));
        }
    }

    @Test(expected = UserNotFoundException.class)
    public void testFindByEmailNegative() throws SQLException {
        @NotNull final String email = "test@email";
        Assert.assertNull(userService.findByEmail(email));
    }

    @Test
    public void testRemoveByLoginPositive() throws SQLException {
        for (@NotNull final UserDTO user : userList) {
            userService.removeByLogin(user.getLogin());
            Assert.assertNull(userService.findById(user.getId()));
        }
    }

    @Test(expected = UserNotFoundException.class)
    public void testRemoveByLoginNegative() throws SQLException {
        @NotNull final String randomId = UUID.randomUUID().toString();
        userService.removeByLogin(randomId);
    }

    @Test
    public void testRemoveByEmailPositive() throws SQLException {
        for (@NotNull final UserDTO user : userList) {
            userService.removeByEmail(user.getEmail());
            Assert.assertNull(userService.findById(user.getId()));
        }
    }

    @Test(expected = UserNotFoundException.class)
    public void testRemoveByEmailNegative() throws SQLException {
        @NotNull final String randomId = UUID.randomUUID().toString();
        userService.removeByEmail(randomId);
    }

    @Test
    public void testSetPassword() throws SQLException {
        @NotNull final String newPassword = "newPassword";
        @NotNull final String oldPassword = userList.get(0).getPasswordHash();
        userService.setPassword(userList.get(0).getId(), newPassword);
        Assert.assertNotEquals(oldPassword, userList.get(0).getPasswordHash());
    }

    @Test
    public void testUpdateUser() throws SQLException {
        @NotNull final String newFirstName = "testFirstName";
        @NotNull final String newLastName = "testLastName";
        @NotNull final String newMiddleName = "testMiddleName";
        userService.updateUser(userList.get(0).getId(), newFirstName, newLastName, newMiddleName);
        Assert.assertEquals(newFirstName, userList.get(0).getFirstName());
        Assert.assertEquals(newLastName, userList.get(0).getLastName());
        Assert.assertEquals(newMiddleName, userList.get(0).getMiddleName());
    }

    @Test
    public void testLockUnlockUserByLogin() throws SQLException {
        userService.lockUserByLogin(userList.get(0).getLogin());
        Assert.assertTrue(userList.get(0).isLocked());
        userService.unlockUserByLogin(userList.get(0).getLogin());
        Assert.assertFalse(userList.get(0).isLocked());
    }

}
