package ru.t1.bugakov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.model.AbstractUserOwnedModelDTO;

import java.sql.SQLException;
import java.util.List;

public interface IAbstractUserOwnedDTORepository<E extends AbstractUserOwnedModelDTO> extends IAbstractDTORepository<E> {
    void add(@NotNull String userId, E entity) throws Exception;

    void update(@NotNull String userId, E entity);

    void remove(@NotNull String userId, E entity);

    void removeById(@NotNull String userId, @NotNull String id);

    boolean existsById(@NotNull String userId, @NotNull String id);

    void clear(@NotNull String userId);

    @Nullable List<E> findAll(@NotNull String userId);

    @Nullable List<E> findAllWithSort(@NotNull String userId, @Nullable String sortField) throws SQLException;

    E findById(@NotNull String userId, @NotNull String id);

    int getSize(@NotNull String userId);

}
