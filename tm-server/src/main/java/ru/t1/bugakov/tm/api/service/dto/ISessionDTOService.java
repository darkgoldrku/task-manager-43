package ru.t1.bugakov.tm.api.service.dto;

import ru.t1.bugakov.tm.dto.model.SessionDTO;

public interface ISessionDTOService extends IAbstractUserOwnedDTOService<SessionDTO> {
}
