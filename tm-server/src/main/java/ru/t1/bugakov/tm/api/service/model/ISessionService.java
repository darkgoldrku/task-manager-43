package ru.t1.bugakov.tm.api.service.model;

import ru.t1.bugakov.tm.model.Session;

public interface ISessionService extends IAbstractUserOwnedService<Session> {
}
