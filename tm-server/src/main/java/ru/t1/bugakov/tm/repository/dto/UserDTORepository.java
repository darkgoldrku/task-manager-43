package ru.t1.bugakov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.dto.IUserDTORepository;
import ru.t1.bugakov.tm.dto.model.UserDTO;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public final class UserDTORepository extends AbstractDTORepository<UserDTO> implements IUserDTORepository {

    public UserDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM UserDTO m";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Nullable
    @Override
    public List<UserDTO> findAll() {
        @NotNull final String jpql = "SELECT m FROM UserDTO m";
        return entityManager.createQuery(jpql, UserDTO.class).getResultList();
    }

    @Nullable
    @Override
    public List<UserDTO> findAllWithSort(@Nullable String sortField) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<UserDTO> criteriaQuery = criteriaBuilder.createQuery(UserDTO.class);
        @NotNull final Root<UserDTO> from = criteriaQuery.from(UserDTO.class);
        criteriaQuery.select(from);
        criteriaQuery.orderBy(criteriaBuilder.asc(from.get(sortField)));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Nullable
    @Override
    public UserDTO findById(@NotNull String id) {
        return entityManager.find(UserDTO.class, id);
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM UserDTO m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult().intValue();
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@NotNull final String login) {
        @NotNull final String jpql = "SELECT m FROM UserDTO m WHERE m.login = :login";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@NotNull final String email) {
        @NotNull final String jpql = "SELECT m FROM UserDTO m WHERE m.email = :email";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public boolean isLoginExists(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(@NotNull final String email) {
        return findByEmail(email) != null;
    }

}


