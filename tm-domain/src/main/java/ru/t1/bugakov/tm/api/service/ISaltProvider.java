package ru.t1.bugakov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ISaltProvider extends IConnectionProvider {

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();

}
